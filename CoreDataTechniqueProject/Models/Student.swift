//
//  Student.swift
//  CoreDataTechniqueProject
//
//  Created by Skyler Bellwood on 7/31/20.
//

import Foundation

struct Student: Hashable {
    let name: String
}
